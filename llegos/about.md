---
layout: default
title: about
description: who is this daniel person, anyway?  🤔
---

![hey, that's me!]({{'/assets/img/daniel.jpg' | absolute_url}})
{: .face}

# Daniel Alejandro Gallegos 

**pronouns:** *`he/him`* or *`they/them`*

> a.k.a "takouhai", "taco", 🌮

...is a Full-Stack Software Developer who works with businesses, governments, [educators](https://web.archive.org/web/20230728005720/https://wvutoday.wvu.edu/media-center-blog/2021/04/14/story-pitch-wvu-students-lead-gender-inclusivity-anti-racism-training), [non-profits](https://carrynaloxone.org/), and [startups](https://web.archive.org/web/20160901215818/https://devacademy.la/) to create digital solutions for your real-world problems, ranging from government enterprise applications to creating graphic assets for professional digital portfolios. After spending a decade working with HTML, CSS, JavaScript, Node.js, Ruby, Java, Linux, Windows, and more, Daniel is more than capable of handling whatever software or hardware problem you need solved. 

Daniel has contributed to projects around the world, including developing enterprise software applications for the Peruvian government, creating on-brand graphic assets for multiple offices at West Virginia University, redesigning an entire technical institute’s website, working as a HIPPA-compliant system administrator for a harm reduction coalitionm and managing servers in the cloud for healthcare corporations. In addition to his extensive software development experience, Daniel is a skilled technical writer, copywriter, graphic designer, and definitely doesn't feel weird about writing himself in the third person.

Daniel holds a Bachelors in Multidisciplinary Studies (BMdS) from [West Virginia University](https://web.archive.org/web/20230728010055/https://lists.wvu.edu/graduation?county=&letter=--&name=&page=5&state=&term=fall&year=2021), which includes areas of study such as: 💻 Computer Science, 🇺🇸 English, 🇪🇸 Spanish, 🇪🇨🇨🇴🇻🇪 Latin American Studies, and 🏳️‍🌈 LGBTQ+ Studies .  He currently works as a [Site Reliability Engineer](https://en.wikipedia.org/wiki/Site_reliability_engineering) at [Zelis](https://www.zelis.com/).

View Daniel's résumé [here]({{'/assets/danielgallegos.resume.pdf' | absolute_url}}){:target="_blank" rel="noreferrer"}.

Want to get in touch? Contact Daniel <a href='ma&#105;&#108;t&#111;&#58;dani&#101;&#37;&#54;C%4&#48;t&#97;&#99;%&#54;F%77%&#54;F&#108;%66&#46;net'>here</a>.
