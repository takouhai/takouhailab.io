---
layout: default
title: home
description: daniel a. gallegos
---

![hey, that's me!]({{'/assets/img/taco.svg' | absolute_url}})
{: .taco}

# 👨‍💻️ daniel alejandro gallegos
**pronouns:** *`he/him`* or *`they/them`*

> a.k.a "takouhai", "taco", 🌮

* 🔮 code alchemist.
* ⭐ digital storyteller.
* 💚 huge nerd.
* [🌮 about](about)
{: .hero}
